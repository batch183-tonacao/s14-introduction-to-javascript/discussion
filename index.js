// console.log("Hello World!") //display output in console

// [SECTION] Syntax, Statements, and Comments

// Statements in programming are instructions that we tell the computer to perform.
// Semicolon to end a statement
// Syntax in programming, it is the set of rules that how statments must be constructed.

/*

There are two type of comments:
1. Single line comments denoted by two slashes (CTRL + /)
2. Multi-line comments denoted by a slash and asterisk (CTRL + SHIFT + /)

*/

// [SECTION] Variables
// It is used to contain data

// Declaring a Variable
// It tells our devices that a variable name is created and is ready to store data

/*
	let/const variableName;

*/

let myVariable;
//  is used to print values of certain variables or result into the browser's console

console.log(myVariable);
//  undefined means the variable's value was not defined.

let hello = "Hi";
console.log(hello);

/*

	Guides in writing variables;
	1. Use the "let" keyword followed by the variable name of your choice and use assignment operator (=) to assign a value
	
	2. Variable names should start with a lowercase character, use camelCase for multiple words.

		one word: variable
		multiple words: myVariable

	3. For constant variable, use the "const" keyword.
	4. Variable names should be indicative (or descriptive) of the value being stored
	to avoid confusion.

*/

		/*
			var vs let/const

			-let and const
				- It cannot be re-declared into the scope/
				= blocked scope or global
			-var
				- It can be re-declared

		*/

// Declaring and initializing
// This is the instance when a variable is given its initial/starting value
		/*
		Syntax:
			let/const variableName = value;

		*/

let productName = "Desktop Computer";
console.log("Product:", productName);

let productPrice = 19988;
console.log("Price:", productPrice);

const interest = 3.539;
console.log("Interest:", interest);

productPrice = productPrice - (productPrice * (interest / 100));
console.log("Discounted Price:", productPrice);

/*
 Reserved keyword as variables
 Using a reserved keyword will cause an error
	const let = "hello";
	console.log(let);

	*/

// [SECTION] Data Types

//  String = are a series of characters that create a word, a phrase, a sentence or anything related to creating a text.
//  JavaScript Strings can be written using either single or double qoutes

//  For other programming languages
// ('a') - Single Character
// ("Hello") - Multiple Character


let country = 'Philippines';
let province = "Metro Manila";

console.log("I live in "+province+", "+country);


/*
// Mini Activity

let firstName = "Kris Daniel", lastName = "Tonacao", fullAddress;
province = "Cebu City";


fullAddress = "Hello! I am " + firstName + " " + lastName +". " + "I live in "+province;
console.log(fullAddress+", "+country);*/

// Escape characters (\) in combination with string characters to produce different result.

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early.';
console.log(message);

// Number
// Integer
let headCount = 26;
console.log("Headcount:", headCount);

// Decimal Number
let grade = 98.7;
console.log("Grade:",grade);

// Exponent
let planetDistance = 2e10;
console.log("Distance:", planetDistance);


// Boolean
// Boolean values are normally used to store values relating to the state of certain things.

let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: "+ isMarried);
console.log("isGoodConduct: "+ isGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
/*
	Syntax:
		let/const arrayName =[elementA, elementB, elementC];

*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

//  Array Index is a number indentifying the place of elements

console.log(grades[2]);

// Object
// are another special kind of data type that's used to mimic real world objects/items.

/*
	Syntax:

		let/const objectName = {
	
			propertyA: value,
			propertyB: value
		}

*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contactNo: ["(0900) 123 - 4567","(032) 123 - 4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person);

//  Select a property of an object
	// using a (.) dot notation

console.log(person.fullName);
console.log(person.address.city);
console.log(person.contactNo[1]);

// Abstract Objects
let myGrades = {
	Mathematics : 98.7,
	Science : 92.1,
	History : 90.2,
	English : 94.6
}

console.log(myGrades);

// Null and Undefined
// Null is an assigned value but it means nothing
// Undefined is a value result from a variable declared without initialization

// let spouse = null;
// console.log(spouse);

// let name;
// console.log(name);

// [SECTION] Functions


// My name is Angelito.
// approach:

/*
let name = John;
console.log("My name is "+ name);

let name1 = Jane;
console.log("My name is "+ name1);
*/

// Declaring a function
	/*
		Syntax:
			function functionName(parameter){
				line/block of codes goes here
			}

		Invoke a function:
		functionName(argument);

		parameter - acts as a named variable/container that only exist in a function.
		argument - is used to pass a value from the invoke function to be used as the parameter.

	*/

function printName(name){
	console.log("My name is "+ name);
	console.log(name);
}

printName("Jane");
printName("John");
console.log(name);

// Using a multiple parameters

function createFullName(firstName,  middleName, lastName){
	console.log("This is parameter firstName: "+firstName);
	console.log("This is parameter middleName: "+middleName);
	console.log("This is parameter lastName: "+lastName);
	
	console.log(firstName,' '+middleName+' '+lastName);
}

createFullName("Juan","Dela", "Cruz");
createFullName("Juan","Dela Cruz");
createFullName("Juan","Dela", "Cruz", "Hello");

let firstName="John";
let middleName="Doe";
let lastName="Smith";

// variable as parameter
createFullName(firstName, middleName, lastName);

// return statement
// It allows the output of the function to be passed to the line/block of code that the function is invoked.

function returnFullName(firstName,middleName,lastName){
	return firstName +' '+middleName+' '+ lastName;
	// line of codes after retuen statement will be disregarded
	console.log("Hello World")
}

let completeName = returnFullName(firstName,middleName,lastName);
console.log("Hi. I am "+completeName);

function myAnimal(animal){
	return "My favorite animal is "+ animal;
}
let myPet = myAnimal("Dog");
console.log(myPet);

console.log("Hi")